package com.supryaga.printer;

/**
 * Created by Игорь on 21.09.2015.
 */
public class Employee<T extends Comparable> implements Comparable<T> {
    private Integer id;
    private String name;
    private int grade;

    public Employee() {
    }

    public Employee(Integer id, String name, int grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        if (grade > 0) {
            this.grade = grade;
        } else {
            throw new IllegalArgumentException("Grade should be greater than 0");
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(T t) {
        Employee employee = (Employee) t;
        if (this.grade > employee.getGrade()) {
            return 1;
        } else if(this.grade == employee.getGrade()) {
            return 0;
        } else {
            return -1;
        }
    }

    @Override
    public String toString() {
        return name + " grade=" + grade;
    }

    public static Employee[] prepareEmployees() {
        final int employeeSize = 4;
        Employee[] employees = new Employee[employeeSize];
        employees[0] = new Employee(1, "Mark", 1);
        employees[1] = new Employee(2, "Peter", 5);
        employees[2] = new Employee(3, "John", 3);
        employees[3] = new Employee(4, "Vasyl", 8);
        return employees;
    }
}
