package com.supryaga.printer;

import com.supryaga.priorityqueue.PriorityQueue;
import com.supryaga.priorityqueue.PriorityQueueImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.whenNew;

/**
 * Created by Игорь on 21.09.2015.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(value = PriorityQueuePrinter.class)
public class PrinterIsolationTest {

    private PriorityQueue priorityQueue;
    private Printer printer = new PriorityQueuePrinter();

    @Before
    public void setUp() throws Exception {
        priorityQueue = mock(PriorityQueueImpl.class);
        whenNew(PriorityQueueImpl.class).withNoArguments().thenReturn((PriorityQueueImpl) priorityQueue);
    }

    @Test(expected = NullPointerException.class)
    public void asSortedStringNull() {
        printer.asSortedString(null);
    }

    @Test
    public void asSortedStringEmpty() {
        assertEquals("", printer.asSortedString(new String[0]));
    }

    @Test
    public void sortedArrAsSortedString() {
        final String sortedStringExp = "6,4,3,2,1,0,-20";
        final int sortedStringSize = 7;
        when(priorityQueue.size()).thenReturn(sortedStringSize);
        when(priorityQueue.popMax()).thenReturn(6, 4, 3, 2, 1, 0, -20);
        String result = printer.asSortedString(6, 4, 3, 2, 1, 0, -20);
        verify(priorityQueue, times(sortedStringSize)).popMax();
        assertEquals(sortedStringExp, result);
    }

    @Test
    public void unSortedArrAsSortedString() {
        final String unsortedStringExp = "6,4,3,2,1";
        final int unsortedStringSize = 5;
        when(priorityQueue.size()).thenReturn(unsortedStringSize);
        when(priorityQueue.popMax()).thenReturn(6, 4, 3, 2, 1);
        String result = printer.asSortedString(1, 4, 6, 3, 2);
        verify(priorityQueue, times(unsortedStringSize)).popMax();
        assertEquals(unsortedStringExp, result);
    }

    @Test
    public void singleElemAsSortedString() {
        final int singleElement = 256;
        final int singleElementSize = 1;
        when(priorityQueue.size()).thenReturn(singleElementSize);
        when(priorityQueue.popMax()).thenReturn(singleElement);
        String result = printer.asSortedString(singleElement);
        verify(priorityQueue, times(singleElementSize)).popMax();
        assertEquals(Integer.toString(singleElement), printer.asSortedString(singleElement));
    }

    @Test
    public void twoElemsAsSortedString() {
        final String twoElementsExp = "512,256";
        final int twoElementsSize = 2;
        when(priorityQueue.size()).thenReturn(twoElementsSize);
        when(priorityQueue.popMax()).thenReturn(512, 256);
        String result = printer.asSortedString(256, 512);
        verify(priorityQueue, times(twoElementsSize)).popMax();
        assertEquals(twoElementsExp, result);
    }

    @Test
    public void doubleValuesAsSortedString() {
        final String doubleValuesExp = "9.0,6.5,6.2,3.8,2.1,1.0";
        final int doubleValuesSize = 6;
        when(priorityQueue.size()).thenReturn(doubleValuesSize);
        when(priorityQueue.popMax()).thenReturn(9.0, 6.5, 6.2, 3.8, 2.1, 1.0);
        String result = printer.asSortedString(6.5,6.2,3.8,2.1,1.0,9.0);
        verify(priorityQueue, times(doubleValuesSize)).popMax();
        assertEquals(doubleValuesExp, result);
    }

    @Test
    public void employeesAsSortedString() {
        Employee[] employees = Employee.prepareEmployees();
        final String sortedEmployeesExp = "Vasyl grade=8,Peter grade=5,John grade=3,Mark grade=1";
        final int employeesSize = employees.length;
        when(priorityQueue.size()).thenReturn(employeesSize);
        when(priorityQueue.popMax()).thenReturn(employees[3], employees[1], employees[2], employees[0]);
        String result = printer.asSortedString(employees);
        verify(priorityQueue, times(employeesSize)).popMax();
        assertEquals(sortedEmployeesExp, result);
    }
}
