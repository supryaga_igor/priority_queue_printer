package com.supryaga.printer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Игорь on 21.09.2015.
 */
public class PrinterIntegrationTest {

    Printer printer;

    @Before
    public void setUp() {
        printer = new PriorityQueuePrinter();
    }

    @Test(expected = NullPointerException.class)
    public void asSortedStringNull() {
        printer.asSortedString(null);
    }

    @Test
    public void asSortedStringEmpty() {
        assertEquals("", printer.asSortedString(new String[0]));
    }

    @Test
    public void sortedArrAsSortedString() {
        final String sortedString = "6,4,3,2,1,0,-20";
        assertEquals(sortedString, printer.asSortedString(6, 4, 3, 2, 1, 0, -20));
    }

    @Test
    public void unSortedArrAsSortedString() {
        final String unsortedString = "6,4,3,2,1";
        assertEquals(unsortedString, printer.asSortedString(1, 4, 6, 3, 2));
    }

    @Test
    public void singleElemAsSortedString() {
        int singleElement = 256;
        assertEquals(Integer.toString(singleElement), printer.asSortedString(singleElement));
    }

    @Test
    public void twoElemsAsSortedString() {
        final String twoElements = "512,256";
        assertEquals(twoElements, printer.asSortedString(256, 512));
    }

    @Test
    public void doubleValuesAsSortedString() {
        final String doubleValues = "9.0,6.5,6.2,3.8,2.1,1.0";
        assertEquals(doubleValues, printer.asSortedString(6.5,6.2,3.8,2.1,1.0,9.0));
    }

    @Test
    public void employeesAsSortedString() {
        final String sortedEmployees = "Vasyl grade=8,Peter grade=5,John grade=3,Mark grade=1";
        Employee[] employees = Employee.prepareEmployees();
        assertEquals(sortedEmployees, printer.asSortedString(employees));
    }
}
