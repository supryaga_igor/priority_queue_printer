package com.supryaga.printer;

import com.supryaga.priorityqueue.PriorityQueue;
import com.supryaga.priorityqueue.PriorityQueueImpl;

/**
 * Created by Игорь on 21.09.2015.
 */
public class PriorityQueuePrinter implements Printer {

    @Override
    public <T extends Comparable<T>> String asSortedString(T... values) {
        PriorityQueue<T> priorityQueue = new PriorityQueueImpl<T>();
        StringBuilder stringBuilder = new StringBuilder();
        for (T value : values) {
            priorityQueue.insert(value);
        }
        int queueSize = priorityQueue.size();
        for (int i = 0; i < queueSize; i++) {
            stringBuilder.append(priorityQueue.popMax());
            if (queueSize - 1 > i) {
                stringBuilder.append(",");
            }
        }
        return stringBuilder.toString();
    }
}
