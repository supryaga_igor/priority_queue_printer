**OTC Technical task**
Application build process must be based on maven. Application must contain two modules named core and util.
util module must contain priority queue class implemented using binary max heap stored in array. Please find details here: http://en.wikipedia.org/wiki/Binary_heap.
Priority queue must be thread safe, but please do not use "synchronized" keyword. Interface for priority queue must be as follows:

```
#!java

public interface PriorityQueue<T extends Comparable<T>> {
/**
* Return number of priority queue elements
*
* @return size of priority queue
*/
int size();
/**
* Inserts new element to priority queue
*
* @param element new element
* @throws NullPointerException when element is null.
*/
void insert(T element);
/**
* Gets maximum element from heap,removes it from the heap.
*
* @return maximum element from heap
* @throws NoSuchElementException if heap is empty or element does not
* exist
*/
T popMax();
}
core module must contain printer class. Printer must use priority queue and must have the following interface:
public interface Printer {
/**
* Returns string with sorted values separated with commas.
* Uses priority queue - first inserts all values and then pops each
* value in priority order and attaches to string
*
* @param values values to sort.
* @param <T> type of value, needs to implement Comparable interface
* @return string with sorted values separated with commas
*/
public <T extends Comparable<T>> String asSortedString(T... values);
}
```

For example invoking printer.asSortedString(1,4,6,3,2) will return string "6,4,3,2,1".
All classes must be written in TDD way and tests must include all border cases. Priority queue must have proper unit tests.
Printer must have proper unit tests (tested in isolation) and integration tests (tested together with priority queue).
While testing Printer in isolation please mock priority queue.
Javadocs and comments in the code are not required - code should be self-descriptive and readable.
**Whenever “must” word is used requirement is mandatory.**