package com.supryaga.priorityqueue;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Игорь on 21.09.2015.
 */
public class PriorityQueueImpl<T extends Comparable<T>> implements PriorityQueue<T> {

    private static final int DEFAULT_CAPACITY = 10;
    private static final int ROOT_INDEX = 0;
    private static final int TRIM_MIN_SIZE = 20;
    private final ReentrantLock lock = new ReentrantLock(true);

    private T[] heap;
    private int size;

    public PriorityQueueImpl() {
        this(DEFAULT_CAPACITY);
    }

    public PriorityQueueImpl(int initialCapacity) {
        heap = (T[]) new Comparable[initialCapacity];
    }

    @Override
    public int size() {
        lock.lock();
        try {
            return size;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void insert(T element) {
        if (element == null) {
            throw new NullPointerException("Should be a non-nullable value");
        }
        lock.lock();
        try {
            if (size == heap.length) {
                doubleSize();
            }
            int i = size++;
            heap[i] = element;
            int parent = (i - 1) / 2;
            while (i > 0 && heap[parent].compareTo(heap[i]) < 0) {
                swapElements(parent, i);
                i = parent;
                parent = (i - 1) / 2;
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public T popMax() {
        lock.lock();
        try {
            if (size <= ROOT_INDEX) {
                throw new NoSuchElementException("PriorityQueue is empty");
            }
            T maxElement = heap[ROOT_INDEX];
            heap[ROOT_INDEX] = heap[size - 1];
            heap[--size] = null;
            heapify(ROOT_INDEX);
            if (size > TRIM_MIN_SIZE && (heap.length - size) > size) {
                heap = Arrays.copyOf(heap, size * 2);
            }
            return maxElement;
        } finally {
            lock.unlock();
        }
    }

    private void heapify(int i) {
        int leftChild;
        int rightChild;
        int largestChild;
        while (true) {
            leftChild = 2 * i + 1;
            rightChild = 2 * i + 2;
            largestChild = i;
            if (leftChild < size && heap[leftChild].compareTo(heap[largestChild]) > 0) {
                largestChild = leftChild;
            }
            if (rightChild < size && heap[rightChild].compareTo(heap[largestChild]) > 0) {
                largestChild = rightChild;
            }
            if (largestChild == i) {
                break;
            }
            swapElements(i, largestChild);
            i = largestChild;
        }
    }

    private void doubleSize() {
        T[] old = heap;
        heap = (T[]) new Comparable[heap.length * 2];
        System.arraycopy(old, ROOT_INDEX, heap, ROOT_INDEX, size);
    }

    private void swapElements(int index1, int index2) {
        T temp = heap[index1];
        heap[index1] = heap[index2];
        heap[index2] = temp;
    }
}
