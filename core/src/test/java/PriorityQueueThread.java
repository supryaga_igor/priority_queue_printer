import com.supryaga.priorityqueue.PriorityQueue;

import java.util.Random;

/**
 * Created by Игорь on 21.09.2015.
 */
public class PriorityQueueThread extends Thread {

    private static final int THREAD_SLEEP_TIME_MAX = 1000;

    private int multiplier;
    private PriorityQueue<Integer> priorityQueue;

    public PriorityQueueThread(PriorityQueue<Integer> priorityQueue, int multiplier) {
        this.priorityQueue = priorityQueue;
        this.multiplier = multiplier;
    }

    @Override
    public void run() {
        Random random = new Random();
        try {
            priorityQueue.insert(1);
            Thread.currentThread().sleep(random.nextInt(THREAD_SLEEP_TIME_MAX));
            for (int i = 1; i < 4; i++) {
                priorityQueue.insert(i * multiplier);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
