import com.supryaga.priorityqueue.PriorityQueue;
import com.supryaga.priorityqueue.PriorityQueueImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by Игорь on 21.09.2015.
 */
public class PriorityQueueTest {

    private PriorityQueue priorityQueue;

    @Before
    public void setUp() {
        priorityQueue = new PriorityQueueImpl();
    }

    @Test
    public void initialSize() {
        assertEquals(0, priorityQueue.size());
    }

    @Test
    public void size() {
        int size = 5;
        priorityQueue.insert("first");
        priorityQueue.insert("second");
        priorityQueue.insert("third");
        priorityQueue.insert("fourth");
        priorityQueue.insert("fifth");
        assertEquals(size, priorityQueue.size());
    }

    @Test(expected = NullPointerException.class)
    public void insertNull() {
        priorityQueue.insert(null);
    }


    @Test
    public void insertEqualElements() {
        priorityQueue.insert("first");
        priorityQueue.insert("first");
        assertEquals(2, priorityQueue.size());
    }

    @Test
    public void popMaxSingleValue() {
        int value = 14;
        priorityQueue.insert(value);
        assertEquals(value, priorityQueue.popMax());
    }

    @Test
    public void popMaxFromEmpty() {
        try {
            priorityQueue.popMax();
            fail("Should throw NoSuchElementException");
        } catch (NoSuchElementException e) {
            assertEquals("PriorityQueue is empty", e.getMessage());
        }
    }

    @Test
    public void popMax() {
        final int value1 = 112;
        final int value2 = 10;
        final int value3 = 1;
        final int value4 = -11;
        int size = 4;
        priorityQueue.insert(value3);
        priorityQueue.insert(value1);
        priorityQueue.insert(value2);
        priorityQueue.insert(value4);
        assertEquals(size, priorityQueue.size());
        assertEquals(value1, priorityQueue.popMax());
        assertEquals(size - 1, priorityQueue.size());
        assertEquals(value2, priorityQueue.popMax());
        assertEquals(size - 2, priorityQueue.size());
        assertEquals(value3, priorityQueue.popMax());
        assertEquals(size - 3, priorityQueue.size());
        assertEquals(value4, priorityQueue.popMax());
    }

    @Test
    public void threadSafetyTest() throws InterruptedException {
        PriorityQueueThread thread1 = new PriorityQueueThread(priorityQueue, 2);
        PriorityQueueThread thread2 = new PriorityQueueThread(priorityQueue, 3);
        PriorityQueueThread thread3 = new PriorityQueueThread(priorityQueue, 4);
        thread1.start();
        thread2.start();
        thread3.start();
        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            throw new InterruptedException(e.getMessage());
        }
        assertEquals(12, priorityQueue.size());
        assertEquals(12, priorityQueue.popMax());
        assertEquals(9, priorityQueue.popMax());
        assertEquals(8, priorityQueue.popMax());
        assertEquals(6, priorityQueue.popMax());
        assertEquals(6, priorityQueue.popMax());
    }
}
